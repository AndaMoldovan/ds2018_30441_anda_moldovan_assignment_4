﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS1
{
    //[Serializable]
    public class Package
    {
        public int Id;
        public int SenderId;
        public int ReceiverId;
        public string Name;
        public string Destination;
        public int SenderCityId;
        public int DestinationCityId;
        public bool Tracing;

        public Package()
        {
        }

        public Package(int id, int senderId, int receiverId, string name, string destination, int senderCityId, int destinationCityId, bool tracing)
        {
            this.Id = id;
            this.SenderId = senderId;
            this.ReceiverId = receiverId;
            this.Name = name;
            this.Destination = destination;
            this.SenderCityId = senderCityId;
            this.DestinationCityId = destinationCityId;
            this.Tracing = tracing;
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}