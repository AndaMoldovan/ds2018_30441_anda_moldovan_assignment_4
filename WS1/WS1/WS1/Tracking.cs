﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS1
{
    public class Tracking
    {
        public int Id;
        public int PackageId;
        public int CityId;
        public DateTime Time;

        public Tracking()
        {
        }

        public Tracking(int id, int packageId, int cityId, DateTime time)
        {
            Id = id;
            PackageId = packageId;
            CityId = cityId;
            Time = time;
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}