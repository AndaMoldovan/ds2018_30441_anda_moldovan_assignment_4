﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS1
{
   

    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }

        public User() { }

        public User(int Id, string Username, string Password, string Role)
        {
            this.Id = Id;
            this.Username = Username;
            this.Password = Password;
            this.Role = Role;
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }

    
}