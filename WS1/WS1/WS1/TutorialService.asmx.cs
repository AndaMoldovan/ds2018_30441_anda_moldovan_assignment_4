﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;


namespace WS1
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class TutorialService : System.Web.Services.WebService
    {


        [WebMethod]
        public string HelloWorld()
        {
            string connStr = "server=localhost;user=root;database=assignment_4;" +
                "port=3306;password=";
            MySqlConnection connection = new MySqlConnection(connStr);

            int id = 0;
            string username = "";
            string pass = "";
            string role = "";
            try
            {
                connection.Open();
                string req = "Select * from user where id=" + 1;
                MySqlCommand cmdDB = new MySqlCommand(req, connection);
                MySqlDataReader rdrDB = cmdDB.ExecuteReader();
                if (rdrDB.HasRows)
                {
                    rdrDB.Read();

                    id = rdrDB.GetInt32("id");
                    username = rdrDB.GetString("username");
                    pass = rdrDB.GetString("password");
                    role = rdrDB.GetString("role");
                }

            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            finally
            {
                connection.Close();
            }
            return "username" + username;
        }


        [WebMethod]
        public string Login(String username, String password)
        {
            string connStr = "server=localhost;user=root;database=assignment_4;" +
                "port=3306;password=";
            MySqlConnection connection = new MySqlConnection(connStr);

            string role = "";
            try
            {
                connection.Open();
                string req = "Select * from user where username=" + username + " and password=" + password;
                MySqlCommand cmdDB = new MySqlCommand(req, connection);
                MySqlDataReader rdrDB = cmdDB.ExecuteReader();
                if (rdrDB.HasRows)
                {
                    rdrDB.Read();
                    if (rdrDB.GetString("username").Equals(username) && rdrDB.GetString("password").Equals(password))
                    {
                        //rdrDB.GetString("role").Equals("ADMIN") ? role = "ADMIN" : role = "USER";
                        if (rdrDB.GetString("role").Equals("ADMIN"))
                        {
                            role = "ADMIN";
                        }
                        else
                        {
                            role = "USER";
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e.ToString());
            }
            finally
            {
                connection.Close();
            }
            return role;
        }

        [WebMethod]
        public bool RegisterUser(string username, string password)
        {
            string connStr = "server=localhost;user=root;database=assignment_4;" +
                    "port=3306;password=";
            MySqlConnection connection = new MySqlConnection(connStr);

            bool result = false;
            try
            {
                connection.Open();
                string req = "Insert into user(username, password, role) values('" + username + "','" + password + "', 'USER')";
                MySqlCommand cmdDB = new MySqlCommand(req, connection);
                cmdDB.ExecuteNonQuery();
                result = true;
            }
            catch (Exception e)
            {
                Console.Write(e.ToString());
            }
            finally
            {
                connection.Close();
            }
            return result;
        }

        [WebMethod]
        public List<Package> GetAllSentPackages(int userId)
        {
            List<Package> result = new List<Package>();

            string connStr = "server=localhost;user=root;database=assignment_4;" +
                    "port=3306;password=";
            MySqlConnection connection = new MySqlConnection(connStr);
            try
            {
                connection.Open();
                string req = "select * from package where sender_id=" + userId;
                MySqlDataAdapter dataAdapter = new MySqlDataAdapter(req, connection);
                DataTable dataTable = new DataTable();
                int recordsAffected = dataAdapter.Fill(dataTable);
                Console.Write("rows affected " + recordsAffected);
                if (recordsAffected > 0)
                {
                    DataTableReader reader = dataTable.CreateDataReader();
                    while (reader.Read())
                    {

                        int id = reader.GetInt32(reader.GetOrdinal("id"));
                        int senderId = reader.GetInt32(reader.GetOrdinal("sender_id"));
                        int receiverId = reader.GetInt32(reader.GetOrdinal("receiver_id"));
                        string name = reader.GetString(reader.GetOrdinal("name"));
                        string destination = reader.GetString(reader.GetOrdinal("destination"));
                        int senderCityId = reader.GetInt32(reader.GetOrdinal("sender_city_id"));
                        int destinationCityId = reader.GetInt32(reader.GetOrdinal("destination_city_id"));
                        bool packageTracing = (reader.GetInt32(reader.GetOrdinal("tracking")) == 1) ? true : false;

                        Package pkg = new Package();
                        pkg.Id = id;
                        pkg.SenderId = senderId;
                        pkg.ReceiverId = receiverId;
                        pkg.Name = name;
                        pkg.Destination = destination;
                        pkg.SenderCityId = senderCityId;
                        pkg.DestinationCityId = destinationCityId;
                        pkg.Tracing = packageTracing;

                        result.Add(pkg);
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e.ToString());
            }
            finally
            {
                connection.Close();
            }

            return result;

        }


        [WebMethod]
        public List<Package> GetAllReceivedPackages(int userId)
        {
            List<Package> result = new List<Package>();

            string connStr = "server=localhost;user=root;database=assignment_4;" +
                    "port=3306;password=";
            MySqlConnection connection = new MySqlConnection(connStr);
            try
            {
                connection.Open();
                string req = "select * from package where receiver_id=" + userId;
                MySqlDataAdapter dataAdapter = new MySqlDataAdapter(req, connection);
                DataTable dataTable = new DataTable();
                int recordsAffected = dataAdapter.Fill(dataTable);
                Console.Write("rows affected " + recordsAffected);
                if (recordsAffected > 0)
                {
                    DataTableReader reader = dataTable.CreateDataReader();
                    while (reader.Read())
                    {

                        int id = reader.GetInt32(reader.GetOrdinal("id"));
                        int senderId = reader.GetInt32(reader.GetOrdinal("sender_id"));
                        int receiverId = reader.GetInt32(reader.GetOrdinal("receiver_id"));
                        string name = reader.GetString(reader.GetOrdinal("name"));
                        string destination = reader.GetString(reader.GetOrdinal("destination"));
                        int senderCityId = reader.GetInt32(reader.GetOrdinal("sender_city_id"));
                        int destinationCityId = reader.GetInt32(reader.GetOrdinal("destination_city_id"));
                        bool packageTracing = (reader.GetInt32(reader.GetOrdinal("tracking")) == 1) ? true : false;

                        Package pkg = new Package();
                        pkg.Id = id;
                        pkg.SenderId = senderId;
                        pkg.ReceiverId = receiverId;
                        pkg.Name = name;
                        pkg.Destination = destination;
                        pkg.SenderCityId = senderCityId;
                        pkg.DestinationCityId = destinationCityId;
                        pkg.Tracing = packageTracing;

                        result.Add(pkg);
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e.ToString());
            }
            finally
            {
                connection.Close();
            }

            return result;

        }

        [WebMethod]
        public Package getPackageById(int packageId)
        {
            Package package = new Package();

            string connStr = "server=localhost;user=root;database=assignment_4;" +
                   "port=3306;password=";
            MySqlConnection connection = new MySqlConnection(connStr);
            try
            {
                connection.Open();
                string req = "Select * from package where id=" + packageId;
                MySqlCommand cmdDB = new MySqlCommand(req, connection);
                MySqlDataReader rdrDB = cmdDB.ExecuteReader();
                if (rdrDB.HasRows)
                {
                    rdrDB.Read();

                    package.Id = rdrDB.GetInt32("id");
                    package.SenderId = rdrDB.GetInt32("sender_id");
                    package.ReceiverId = rdrDB.GetInt32("receiver_id");
                    package.Name = rdrDB.GetString("name");
                    package.Destination = rdrDB.GetString("destination");
                    package.SenderCityId = rdrDB.GetInt32("sender_city_id");
                    package.DestinationCityId = rdrDB.GetInt32("destinaton_city_id");
                    package.Tracing = (rdrDB.GetInt32("tracking") == 1) ? true : false;
                }

            }
            catch (Exception e)
            {
                Console.Write(e.ToString());
            }
            finally
            {
                connection.Close();
            }
            return package;

        }

        [WebMethod]
        public Package getPackageByName(string packageName)
        {
            Package package = new Package();

            string connStr = "server=localhost;user=root;database=assignment_4;" +
                   "port=3306;password=";
            MySqlConnection connection = new MySqlConnection(connStr);
            try
            {
                connection.Open();
                string req = "Select * from package where name=" + packageName;
                MySqlCommand cmdDB = new MySqlCommand(req, connection);
                MySqlDataReader rdrDB = cmdDB.ExecuteReader();
                if (rdrDB.HasRows)
                {
                    rdrDB.Read();

                    package.Id = rdrDB.GetInt32("id");
                    package.SenderId = rdrDB.GetInt32("sender_id");
                    package.ReceiverId = rdrDB.GetInt32("receiver_id");
                    package.Name = rdrDB.GetString("name");
                    package.Destination = rdrDB.GetString("destination");
                    package.SenderCityId = rdrDB.GetInt32("sender_city_id");
                    package.DestinationCityId = rdrDB.GetInt32("destinaton_city_id");
                    package.Tracing = (rdrDB.GetInt32("tracking") == 1) ? true : false;
                }

            }
            catch (Exception e)
            {
                Console.Write(e.ToString());
            }
            finally
            {
                connection.Close();
            }
            return package;

        }

        [WebMethod]
        public List<Tracking> getPackageStatus(int packageId)
        {
            List<Tracking> result = new List<Tracking>();

            string connStr = "server=localhost;user=root;database=assignment_4;" +
                   "port=3306;password=";
            MySqlConnection connection = new MySqlConnection(connStr);
            try
            {
                connection.Open();
                string req = "Select * from tracking where package_id=" + packageId;
                MySqlDataAdapter adapter = new MySqlDataAdapter(req, connection);
                DataTable dataTable = new DataTable();
                int recordsAffected = adapter.Fill(dataTable);
                if(recordsAffected > 0)
                {
                    DataTableReader reader = dataTable.CreateDataReader();
                    while (reader.Read())
                    {
                        Tracking tracking = new Tracking();
                        tracking.Id = reader.GetInt32(reader.GetOrdinal("id"));
                        tracking.PackageId = reader.GetInt32(reader.GetOrdinal("package_id"));
                        tracking.CityId = reader.GetInt32(reader.GetOrdinal("city_id"));
                        tracking.Time = reader.GetDateTime(reader.GetOrdinal("time"));

                        result.Add(tracking);
                    }
                }

            }
            catch (Exception e)
            {
                Console.Write(e.ToString());
            }
            finally
            {
                connection.Close();
            }
            return result;

        }

    }

}

