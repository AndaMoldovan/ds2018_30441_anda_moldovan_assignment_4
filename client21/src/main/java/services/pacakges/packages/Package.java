
package services.pacakges.packages;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for package complex type.
 * 
 * <p>The following schema fragment specifies the expected         content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="package"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="destination" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="destinationCityId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="receiverId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="senderCityId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="senderId" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="tracking" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "package", propOrder = {
    "destination",
    "destinationCityId",
    "id",
    "name",
    "receiverId",
    "senderCityId",
    "senderId",
    "tracking"
})
public class Package {

    protected String destination;
    protected int destinationCityId;
    protected int id;
    protected String name;
    protected int receiverId;
    protected int senderCityId;
    protected int senderId;
    protected int tracking;

    /**
     * Gets the value of the destination property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestination() {
        return destination;
    }

    /**
     * Sets the value of the destination property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestination(String value) {
        this.destination = value;
    }

    /**
     * Gets the value of the destinationCityId property.
     * 
     */
    public int getDestinationCityId() {
        return destinationCityId;
    }

    /**
     * Sets the value of the destinationCityId property.
     * 
     */
    public void setDestinationCityId(int value) {
        this.destinationCityId = value;
    }

    /**
     * Gets the value of the id property.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the receiverId property.
     * 
     */
    public int getReceiverId() {
        return receiverId;
    }

    /**
     * Sets the value of the receiverId property.
     * 
     */
    public void setReceiverId(int value) {
        this.receiverId = value;
    }

    /**
     * Gets the value of the senderCityId property.
     * 
     */
    public int getSenderCityId() {
        return senderCityId;
    }

    /**
     * Sets the value of the senderCityId property.
     * 
     */
    public void setSenderCityId(int value) {
        this.senderCityId = value;
    }

    /**
     * Gets the value of the senderId property.
     * 
     */
    public int getSenderId() {
        return senderId;
    }

    /**
     * Sets the value of the senderId property.
     * 
     */
    public void setSenderId(int value) {
        this.senderId = value;
    }

    /**
     * Gets the value of the tracking property.
     * 
     */
    public int getTracking() {
        return tracking;
    }

    /**
     * Sets the value of the tracking property.
     * 
     */
    public void setTracking(int value) {
        this.tracking = value;
    }

}
