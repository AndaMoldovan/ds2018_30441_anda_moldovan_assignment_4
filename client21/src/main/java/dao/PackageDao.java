package dao;

import entities.Package;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class PackageDao {

    private SessionFactory sessionFactory;

    public PackageDao(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    public List<Package> getAllPackages(){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<Package> result = new ArrayList<Package>();
        try{
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Package");
            result = query.list();
            tx.commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }

        return result;
    }

    public Package getPackageById(int id){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<Package> result = new ArrayList<Package>();
        try{
            tx = session.beginTransaction();
            //noinspection JpaQlInspection
            String req = "FROM Package WHERE id=:packageId";
            Query query = session.createQuery(req);
            query.setParameter("packageId", id);
            result = query.list();
            tx.commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }
        return (result != null && !result.isEmpty()) ? result.get(0) : null;
    }

    public List<Package> getPackagesBySenderId(int senderId){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<Package> result = new ArrayList<Package>();
        try{
            tx = session.beginTransaction();
            //noinspection JpaQlInspection
            String req = "FROM Package WHERE sender_id=:senderId";
            Query query = session.createQuery(req);
            query.setParameter("senderId", senderId);
            result = query.list();
            tx.commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }
        return result;
    }

    public List<Package> getPackagesByReceiverId(int receiverId){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<Package> result = new ArrayList<Package>();
        try{
            tx = session.beginTransaction();
            //noinspection JpaQlInspection
            String req = "FROM Package WHERE receiver_id=:receiverId";
            Query query = session.createQuery(req);
            query.setParameter("receiverId", receiverId);
            result = query.list();
            tx.commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }
        return result;
    }

    public Package getPackageByName(String packageName){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<Package> packages = null;
        try{
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Package WHERE name=:packageName");
            query.setParameter("packageName", packageName);
            packages = query.list();
            tx.commit();
        } catch (HibernateException e){
            if(tx != null){
                tx.rollback();
            }
        }
        return  packages != null && !packages.isEmpty() ? packages.get(0) : null;
    }

    public Package addPackage(Package pkg){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        int packageId = - 1;
        try{
            tx = session.beginTransaction();
            packageId = (Integer) session.save(pkg);
            pkg.setId(packageId);
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
        }
        return (packageId != -1) ? pkg : null;
    }

    public Package updatePackage(Package pkg){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        Package cPackage = null;
        try{
            tx = session.beginTransaction();
            session.update(pkg);
            cPackage = getPackageById(pkg.getId());
            tx.commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }
        return (cPackage != null) ? cPackage : null;
    }

    public boolean deletePackage(int packageId) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        Package pkg = getPackageById(packageId);
        try{
            tx = session.beginTransaction();
            session.delete(pkg);
            tx.commit();
            return true;
        }catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }
}
