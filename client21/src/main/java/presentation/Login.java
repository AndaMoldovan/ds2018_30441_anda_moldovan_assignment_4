package presentation;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import services.pacakges.user.User;
import services.pacakges.user.UserService;
import services.pacakges.user.UserServiceImplService;

public class Login extends Application {

    private Stage window;
    private Scene scene, administratorScene, userScene;

    private static UserServiceImplService userServiceImplService;
    private static UserService userService;

    static{
        userServiceImplService = new UserServiceImplService();
        userService = userServiceImplService.getUserServiceImplPort();
    }

    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;

        GridPane mainGrid = new GridPane();
        mainGrid.setAlignment(Pos.CENTER);
        mainGrid.setHgap(10);
        mainGrid.setVgap(10);
        mainGrid.setPadding(new Insets(25, 25, 25, 25));


        Text mainSceneTitle = new Text("Welcome!");
        mainSceneTitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));

        mainGrid.add(mainSceneTitle, 1, 0, 2, 1);

        Label usernameLabel = new Label("username");
        Label passwordLabel = new Label("password");

        final TextField usernameTextField = new TextField();
        final TextField passwordTextField = new TextField();
        Button loginBtn = new Button("Log in");
        Button registerBtn = new Button("Register");

        mainGrid.add(usernameLabel, 0, 1, 1,1);
        mainGrid.add(passwordLabel, 0, 2, 1,1);
        mainGrid.add(usernameTextField, 1, 1, 1,1);
        mainGrid.add(passwordTextField , 1, 2, 1,1);
        mainGrid.add(loginBtn, 0, 3, 2,1);
        mainGrid.add(registerBtn, 1, 3, 2,1);

        GridPane adminGrid = new GridPane();
        adminGrid.setAlignment(Pos.CENTER);
        adminGrid.setHgap(10);
        adminGrid.setVgap(10);
        adminGrid.setPadding(new Insets(25, 25, 25, 25));

        Text adminSceneTitle = new Text("Welcome Administrator!");
        adminSceneTitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        administratorScene = new Scene(adminGrid, 1000, 500);
        adminGrid.add(adminSceneTitle, 2,  0, 3, 1);

        GridPane userGrid = new GridPane();
        userGrid.setAlignment(Pos.CENTER);
        userGrid.setHgap(10);
        userGrid.setVgap(10);
        userGrid.setPadding(new Insets(25, 25, 25, 25));

        Text userSceneTitle = new Text("Welcome User!");
        userSceneTitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        userScene = new Scene(userGrid, 1000, 500);
        userGrid.add(userSceneTitle, 2,  0, 3, 1);

        loginBtn.setOnAction(e -> handleLogIn(usernameTextField, passwordTextField, adminGrid, userGrid));
        registerBtn.setOnAction(e -> handleRegister(usernameTextField, passwordTextField));

        scene = new Scene(mainGrid, 400, 400);
        window.setScene(scene);
        window.show();
    }

    private void handleRegister(TextField usernameTextField, TextField passwordTextField) {
        userService.addUser(usernameTextField.getText(), passwordTextField.getText());
    }

    private void handleLogIn(TextField usernameTextField, TextField passwordTextField, GridPane adminGrid, GridPane userGrid) {
        User user = userService.login(usernameTextField.getText(), passwordTextField.getText());
        if(user != null){
            if(user.getRole().equals("ADMIN")){
                Administrator admin = new Administrator(administratorScene, adminGrid, user);
                System.out.println("\n\nADMIN\n\n");
                window.setScene(administratorScene);
            }else if(user.getRole().equals("USER")){
                UserPresentation userPresentation = new UserPresentation(userScene, userGrid, user);
                window.setScene(userScene);
            }
        }


    }
}
