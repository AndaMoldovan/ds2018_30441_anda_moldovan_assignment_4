package presentation;

import dao.CityDao;
import dao.PackageDao;
import dao.TrackingDao;
import dao.UserDao;
import entities.City;
import entities.Tracking;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import services.pacakges.user.User;
import entities.Package;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class Administrator {

    private Scene scene;
    private GridPane grid;


    ListView<String> packages;
    ListView<String> tracking;

    private static SessionFactory sessionFactory;
    private static UserDao userDao;
    private static PackageDao packageDao;
    private static CityDao cityDao;
    private static TrackingDao trackingDao;


    private User user;
    static{
        sessionFactory = new Configuration().configure().buildSessionFactory();
        userDao = new UserDao(sessionFactory);
        packageDao = new PackageDao(sessionFactory);
        cityDao = new CityDao(sessionFactory);
        trackingDao = new TrackingDao(sessionFactory);
    }

    public Administrator(Scene scene, GridPane grid, User user){
        this.scene = scene;
        this.grid = grid;
        this.user = user;

        packages = new ListView<>();
        tracking = new ListView<>();
        arrangeScene();
    }

    private void arrangeScene(){

        grid.add(new Label("All Packages"), 0, 1);

        grid.add(packages, 0, 2, 4,7);
        packages.setPrefSize(400, 200);
        initializePackageTable();

        Label senderIdLbl = new Label("senderId");
        Label receiverIdLbl = new Label("receiverId");
        Label nameLbl = new Label("name");
        Label descriptionLbl = new Label("description");
        Label senderCityIdLbl = new Label("senderCityId");
        Label receiverCityIdLbl = new Label("receiverCityId");
        Label trackingLbl = new Label("tracking");

        TextField senderIdTxt = new TextField();
        TextField receiverIdTxt = new TextField();
        TextField nameTxt = new TextField();
        TextField descriptionTxt = new TextField();
        TextField senderCityTxt = new TextField();
        TextField receiverCityTxt = new TextField();
        TextField trackingTxt = new TextField();

        grid.add(senderIdLbl, 5,2,1,1);
        grid.add(senderIdTxt, 6,2,1,1);
        grid.add(receiverIdLbl, 5,3,1,1);
        grid.add(receiverIdTxt, 6,3,1,1);
        grid.add(nameLbl, 5,4,1,1);
        grid.add(nameTxt, 6,4,1,1);
        grid.add(descriptionLbl, 5,5,1,1);
        grid.add(descriptionTxt, 6,5,1,1);
        grid.add(senderCityIdLbl, 5,6,1,1);
        grid.add(senderCityTxt, 6,6,1,1);
        grid.add(receiverCityIdLbl, 5,7,1,1);
        grid.add(receiverCityTxt, 6,7,1,1);
        grid.add(trackingLbl, 5,8,1,1);
        grid.add(trackingTxt, 6,8,1,1);

        Button addPackageBtn = new Button("Add Package");
        grid.add(addPackageBtn, 7, 8, 1, 1);
        Button removePackageBtn = new Button("Remove Package");
        grid.add(removePackageBtn, 7, 7, 1, 1);


        Label registerPackageLbl = new Label("Register for tracking");
        TextField registerTxt = new TextField();
        Button registerBtn = new Button("Register package");

        grid.add(registerPackageLbl, 0, 9, 1, 1);
        grid.add(registerTxt, 1, 9,1, 1);
        grid.add(registerBtn, 2, 9, 1, 1);

        Label showTrackingLbl = new Label("Set Tracking");
        TextField showTrackingTxt = new TextField();
        Label showTrackingCityLbl = new Label("City");
        TextField showTrackingCityTxt = new TextField();
        Label showTrackingTimeLbl = new Label("Time");
        TextField showTrackingTimeTxt = new TextField();
        Button showTrackingBtn = new Button("Register package");

        grid.add(showTrackingLbl, 0, 10, 1, 1);
        grid.add(showTrackingTxt, 1, 10,1, 1);
        grid.add(showTrackingCityLbl, 0, 11, 1, 1);
        grid.add(showTrackingCityTxt, 1, 11,1, 1);
        grid.add(showTrackingTimeLbl, 0, 12, 1, 1);
        grid.add(showTrackingTimeTxt, 1, 12,1, 1);
        grid.add(showTrackingBtn, 2, 10, 1, 1);

        grid.add(tracking, 0, 13, 4,3);
        tracking.setPrefSize(200, 300);
        initializeTracking(-1);

        Label showTrForPkgLbl = new Label("Show Tracking");
        TextField showTrForPkgTxt = new TextField();
        Button showTrForPkgBtn = new Button("Load");

        grid.add(showTrForPkgLbl, 5, 13, 1, 1);
        grid.add(showTrForPkgTxt, 6, 13,1, 1);
        grid.add(showTrForPkgBtn, 7, 13, 1, 1);

        addPackageBtn.setOnAction(e -> handleAddPackage(senderIdTxt, receiverIdTxt, nameTxt, descriptionTxt,senderCityTxt, receiverCityTxt, trackingTxt));
        removePackageBtn.setOnAction(e -> handleRemovePackage(nameTxt));
        registerBtn.setOnAction(e -> handleRegisterPackage(registerTxt));
        showTrackingBtn.setOnAction(e -> handleAddTracking(showTrackingTxt, showTrackingCityTxt, showTrackingTimeTxt));
        showTrForPkgBtn.setOnAction(e -> handleFillTrackingTable(showTrForPkgTxt));
    }

    private void handleFillTrackingTable(TextField showTrForPkgTxt) {
        int packageId = (Integer.parseInt(showTrForPkgTxt.getText()));
        initializeTracking(packageId);
    }

    private void handleAddTracking(TextField showTrackingTxt, TextField showTrackingCityTxt, TextField showTrackingTimeTxt) {
        Tracking tr = new Tracking();
        tr.setPackageId(Integer.parseInt(showTrackingTxt.getText()));
        tr.setCityId(Integer.parseInt(showTrackingCityTxt.getText()));


        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Date startDate = df.parse(showTrackingTimeTxt.getText());
            tr.setTime(startDate);
            trackingDao.addTracking(tr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    private void handleRegisterPackage(TextField registerTxt) {
        int pacakgeId = Integer.parseInt(registerTxt.getText());

        Package pkg = packageDao.getPackageById(pacakgeId);
        pkg.setTracking(1);

        packageDao.updatePackage(pkg);
    }

    private void handleRemovePackage(TextField nameTxt) {
        Package pkg = packageDao.getPackageByName(nameTxt.getText());
        packageDao.deletePackage(pkg.getId());
        initializePackageTable();
    }

    private void handleAddPackage(TextField senderIdTxt, TextField receiverIdTxt, TextField nameTxt, TextField descriptionTxt, TextField senderCityTxt, TextField receiverCityTxt, TextField trackingTxt) {
        Package pkg = new Package();
        pkg.setSenderId(Integer.parseInt(senderIdTxt.getText()));
        pkg.setReceiverId(Integer.parseInt(receiverIdTxt.getText()));
        pkg.setName(nameTxt.getText());
        pkg.setDestination(descriptionTxt.getText());
        pkg.setSenderCityId(Integer.parseInt(senderCityTxt.getText()));
        pkg.setDestinationCityId(Integer.parseInt(receiverCityTxt.getText()));
        pkg.setTracking(Integer.parseInt(trackingTxt.getText()));

        packageDao.addPackage(pkg);
        initializePackageTable();
    }


    private void initializePackageTable() {

        List<entities.Package> list = packageDao.getAllPackages();
        List<Object[]> result = new ArrayList<>();
        ObservableList<String> sold = FXCollections.<String>observableArrayList();

        packages.getItems().clear();

        for(entities.Package pkg : list) {
            entities.City senderCity = cityDao.getCityById(pkg.getSenderCityId());
            City destinationCity = cityDao.getCityById(pkg.getDestinationCityId());
            entities.User sender = userDao.getUserById(pkg.getSenderId());
            entities.User receiver = userDao.getUserById(pkg.getReceiverId());

            String tracking = (pkg.getTracking() == 1) ? "true" : "false";
//            Object[] obj = {sender.getUsername(), receiver.getUsername(), pkg.getName(),
//                     pkg.getDestination(), senderCity.getName(), destinationCity.getName(), tracking};
//            result.add(obj);

            sold.add("Sender: " + sender.getUsername() + " Receiver: " + receiver.getUsername() +
                    " Package Name: " + pkg.getName() + " Destination: " + pkg.getDestination() +
                    " Sender City: " + senderCity.getName() + " Receiver City: " + destinationCity.getName() +
                    " Tracking: " + tracking);
        }
        packages.getItems().addAll(sold);

    }

    private void initializeTracking(int packageId){
        if(packageId > -1){
            List<Tracking> list = trackingDao.getTrackingByPackageId(packageId);
            ObservableList<String> sold = FXCollections.<String>observableArrayList();

            tracking.getItems().clear();
            for(Tracking tr: list){
                sold.add("Package Id: " + tr.getPackageId() + " City Id: " + tr.getCityId() + " Time: " + tr.getTime());
            }
            tracking.getItems().addAll(sold);
        }

    }

}

