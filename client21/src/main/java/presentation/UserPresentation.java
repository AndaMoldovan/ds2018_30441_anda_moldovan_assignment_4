package presentation;

import dao.CityDao;
import dao.PackageDao;
import dao.TrackingDao;
import dao.UserDao;
import entities.City;
import entities.Package;
import entities.Tracking;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import services.pacakges.user.User;

import java.util.ArrayList;
import java.util.List;

public class UserPresentation {
    private Scene scene;
    private GridPane grid;

    ListView<String> packages;
    ListView<String> tracking;

    private static SessionFactory sessionFactory;
    private static UserDao userDao;
    private static PackageDao packageDao;
    private static CityDao cityDao;
    private static TrackingDao trackingDao;

    private User user;
    static{
        sessionFactory = new Configuration().configure().buildSessionFactory();
        userDao = new UserDao(sessionFactory);
        packageDao = new PackageDao(sessionFactory);
        cityDao = new CityDao(sessionFactory);
        trackingDao = new TrackingDao(sessionFactory);
    }

    public UserPresentation(Scene scene, GridPane grid, User user){
        this.scene = scene;
        this.grid = grid;
        this.user = user;

        packages = new ListView<>();
        tracking = new ListView<>();

        arrangeScene();
    }

    private void arrangeScene() {
        grid.add(new Label("List All Packages"), 0, 1);

        grid.add(packages, 0, 2, 4,7);
        packages.setPrefSize(400, 200);
        initializePackageTable();

        Label searchPackageLbl = new Label("Search for Package");
        searchPackageLbl.setPrefSize(120, 60);
        TextField searchPackageTxt = new TextField();
        Button searchBtn = new Button("Search");

        grid.add(searchPackageLbl, 5, 2, 2,1);
        grid.add(searchPackageTxt, 7, 2, 1,1);
        grid.add(searchBtn, 8, 2, 1, 1);

        grid.add(tracking, 0, 9, 4,3);
        tracking.setPrefSize(200, 300);
        initializeTracking(-1);


        Label searchTrackingLbl = new Label("Track a package");
        searchTrackingLbl.setPrefSize(120, 60);
        TextField searchTrackingTxt = new TextField();
        Button searchTrackingBtn = new Button("Search");

        grid.add(searchTrackingLbl, 0, 14, 2,1);
        grid.add(searchTrackingTxt, 2, 14, 1,1);
        grid.add(searchTrackingBtn, 3, 15, 1, 1);

        searchBtn.setOnAction(e -> handleSearch(searchPackageTxt));
        searchTrackingBtn.setOnAction(e -> handleTrackPacket(searchTrackingTxt));

    }

    private void handleTrackPacket(TextField searchTrackingTxt) {
        int packageId = Integer.parseInt(searchTrackingTxt.getText());
        initializeTracking(packageId);
    }

    private void initializeTracking(int packageId) {
        if(packageId > -1){
            List<Tracking> list = trackingDao.getTrackingByPackageId(packageId);
            ObservableList<String> sold = FXCollections.<String>observableArrayList();

            tracking.getItems().clear();
            for(Tracking tr: list){
                sold.add("Package Id: " + tr.getPackageId() + " City Id: " + tr.getCityId() + " Time: " + tr.getTime());
            }
            tracking.getItems().addAll(sold);
        }
    }

    private void handleSearch(TextField searchPackageTxt) {
        Package pkg = packageDao.getPackageByName(searchPackageTxt.getText());
        initializePackageTable(pkg);
    }

    private void initializePackageTable(Package pkg) {
        ObservableList<String> sold = FXCollections.<String>observableArrayList();
        packages.getItems().clear();

        entities.City senderCity = cityDao.getCityById(pkg.getSenderCityId());
        City destinationCity = cityDao.getCityById(pkg.getDestinationCityId());
        entities.User sender = userDao.getUserById(pkg.getSenderId());
        entities.User receiver = userDao.getUserById(pkg.getReceiverId());

        String tracking = (pkg.getTracking() == 1) ? "true" : "false";

        sold.add("Sender: " + sender.getUsername() + " Receiver: " + receiver.getUsername() +
                " Package Name: " + pkg.getName() + " Destination: " + pkg.getDestination() +
                " Sender City: " + senderCity.getName() + " Receiver City: " + destinationCity.getName() +
                " Tracking: " + tracking);
        packages.getItems().addAll(sold);
    }

    private void initializePackageTable() {

        List<Package> list = packageDao.getPackagesBySenderId(user.getId());
        list.addAll(packageDao.getPackagesByReceiverId(user.getId()));
        ObservableList<String> sold = FXCollections.<String>observableArrayList();

        packages.getItems().clear();

        for(entities.Package pkg : list) {
            entities.City senderCity = cityDao.getCityById(pkg.getSenderCityId());
            City destinationCity = cityDao.getCityById(pkg.getDestinationCityId());
            entities.User sender = userDao.getUserById(pkg.getSenderId());
            entities.User receiver = userDao.getUserById(pkg.getReceiverId());

            String tracking = (pkg.getTracking() == 1) ? "true" : "false";

            sold.add("Sender: " + sender.getUsername() + " Receiver: " + receiver.getUsername() +
                    " Package Name: " + pkg.getName() + " Destination: " + pkg.getDestination() +
                    " Sender City: " + senderCity.getName() + " Receiver City: " + destinationCity.getName() +
                    " Tracking: " + tracking);
        }
        packages.getItems().addAll(sold);

    }
}
