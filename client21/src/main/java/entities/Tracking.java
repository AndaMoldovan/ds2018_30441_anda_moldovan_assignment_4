package entities;

import java.sql.Timestamp;
import java.util.Date;

public class Tracking {

    private int id;
    private int packageId;
    private int cityId;
    private Date time;

    public Tracking() {}

    public Tracking(int id, int packageId, int cityId, Date time) {
        this.id = id;
        this.packageId = packageId;
        this.cityId = cityId;
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Tracking{" +
                "id=" + id +
                ", packageId=" + packageId +
                ", cityId=" + cityId +
                ", time=" + time +
                '}';
    }
}
