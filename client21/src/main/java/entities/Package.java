package entities;

public class Package {

    private int id;
    private int senderId;
    private int receiverId;
    private String name;
    private String destination;
    private int senderCityId;
    private int destinationCityId;
    private int tracking;

    public Package() {}

    public Package(int id, int senderId, int receiverId, String name, String destination, int senderCityId, int destinationCityId, int tracking) {
        this.id = id;
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.name = name;
        this.destination = destination;
        this.senderCityId = senderCityId;
        this.destinationCityId = destinationCityId;
        this.tracking = tracking;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSenderId() {
        return senderId;
    }

    public void setSenderId(int senderId) {
        this.senderId = senderId;
    }

    public int getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(int receiverId) {
        this.receiverId = receiverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getSenderCityId() {
        return senderCityId;
    }

    public void setSenderCityId(int senderCityId) {
        this.senderCityId = senderCityId;
    }

    public int getDestinationCityId() {
        return destinationCityId;
    }

    public void setDestinationCityId(int destinationCityId) {
        this.destinationCityId = destinationCityId;
    }

    public int getTracking() {
        return tracking;
    }

    public void setTracking(int tracking) {
        this.tracking = tracking;
    }

    @Override
    public String toString() {
        return "Package{" +
                "id=" + id +
                ", senderId=" + senderId +
                ", receiverId=" + receiverId +
                ", name='" + name + '\'' +
                ", destination='" + destination + '\'' +
                ", senderCityId=" + senderCityId +
                ", destinationCityId=" + destinationCityId +
                ", tracking=" + tracking +
                '}';
    }
}
