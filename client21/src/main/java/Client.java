import dao.CityDao;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import services.pacakges.cities.ArrayList;
import services.pacakges.cities.City;
import services.pacakges.cities.CityService;
import services.pacakges.cities.CityServiceImplService;

public class Client {

    public static void main(String[] args){
        CityServiceImplService cityServiceImplService = new CityServiceImplService();
        CityService cityService = cityServiceImplService.getCityServiceImplPort();

        ArrayList list = cityService.getAllCities();

        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

        CityDao cityDao = new CityDao(sessionFactory);
        System.out.println(cityDao.getCityById(1));

    }
}
