package testpackage;

import javax.jws.WebService;

@WebService(endpointInterface = "testpackage.HelloWorld")
public class HelloWorldImpl implements HelloWorld {
    @Override
    public String getHelloWorldMessage() {
        return "Hello World Anda";
    }
}
