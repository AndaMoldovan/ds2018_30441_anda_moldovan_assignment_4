import dao.CityDao;
import dao.PackageDao;
import dao.TrackingDao;
import dao.UserDao;
import entities.City;
import entities.Package;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Test {

    private static UserDao userDao;
    private static PackageDao packageDao;
    private static CityDao cityDao;
    private static TrackingDao trackingDao;
    private static SessionFactory sessionFactory;

    public static void main(String[] args){
        sessionFactory = new Configuration().configure().buildSessionFactory();
        userDao = new UserDao(sessionFactory);
        System.out.println(userDao.getUserById(1).toString());

        packageDao = new PackageDao(sessionFactory);
        System.out.println(packageDao.getPackagesBySenderId(1));

        cityDao = new CityDao(sessionFactory);
        System.out.println(cityDao.getCityById(4));

        trackingDao = new TrackingDao(sessionFactory);
        System.out.println(trackingDao.getTrackingById(1));
    }
}
