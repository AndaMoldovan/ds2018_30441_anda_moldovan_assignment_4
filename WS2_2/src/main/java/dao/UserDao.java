package dao;


import java.util.ArrayList;
import java.util.List;
import entities.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class UserDao {

    private SessionFactory sessionFactory;

    public UserDao(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    public User getUserById(int id){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<User> result = new ArrayList<User>();
        try{
            tx = session.beginTransaction();
            //noinspection JpaQlInspection
            String req = "FROM User WHERE id=:userId";
            Query query = session.createQuery(req);
            query.setParameter("userId", id);
            result = query.list();
            tx.commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }
        return (result != null && !result.isEmpty()) ? result.get(0) : null;
    }

    public User getUserByUsernameAndPassword(String username, String password){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<User> result = new ArrayList<User>();
        try{
            tx = session.beginTransaction();
            //noinspection JpaQlInspection
            String req = "FROM User WHERE username=:name AND password=:pass";
            Query query = session.createQuery(req);
            query.setParameter("name", username);
            query.setParameter("pass", password);
            result = query.list();
            tx.commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }
        return (result != null && !result.isEmpty()) ? result.get(0) : null;
    }

    public User addUser(User user){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        int userId = -1;
        user.setRole("USER");
        try{
            tx = session.beginTransaction();
            userId = (Integer)session.save(user);
            user.setId(userId);
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
        }
        return (userId != -1) ? user : null;
    }

}
