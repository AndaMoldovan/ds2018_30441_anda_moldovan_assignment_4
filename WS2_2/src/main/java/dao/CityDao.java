package dao;

import entities.City;
import org.hibernate.*;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class CityDao {

    private SessionFactory sessionFactory;

    public CityDao(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    public List<City> getAllCities(){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<City> result = new ArrayList<City>();
        try{
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM City");
            result = query.list();
            tx.commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }

        return result;
    }

    public City getCityById(int id){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<City> result = new ArrayList<City>();
        try{
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM City WHERE id=:cityId");
            query.setParameter("cityId", id);
            result = query.list();
            tx.commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }
        return (result != null && !result.isEmpty()) ? result.get(0) : null;
    }

    public City addCity(City city){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        int cityId = -1;
        try{
            tx = session.beginTransaction();
            cityId = (Integer)session.save(city);
            city.setId(cityId);
            tx.commit();
        }catch (HibernateException e){
            e.printStackTrace();
        }
        return (cityId != -1) ? city : null;
    }

    public City updateCity(City city){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        City newCity = null;
        try{
            tx = session.beginTransaction();
            session.update(city);
            newCity = getCityById(city.getId());
            tx.commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }
        return (newCity != null) ? newCity : null;
    }

    public boolean deleteCity(int id){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        City city = getCityById(id);
        try{
            tx = session.beginTransaction();
            session.delete(city);
            tx.commit();
            return  true;
        }catch(HibernateException e){
            e.printStackTrace();
        }
        return false;
    }
}
