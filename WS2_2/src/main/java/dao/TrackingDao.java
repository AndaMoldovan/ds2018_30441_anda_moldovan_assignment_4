package dao;

import entities.Tracking;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class TrackingDao {

    private SessionFactory sessionFactory;

    public TrackingDao(SessionFactory sessionFactory){this.sessionFactory = sessionFactory;}

    public List<Tracking> getAllTrackings(){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<Tracking> result = new ArrayList<Tracking>();
        try{
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Tracking");
            result = query.list();
            tx.commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }
        return result;
    }

    public Tracking getTrackingById(int id){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<Tracking> result = new ArrayList<Tracking>();
        try{
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Tracking WHERE id=:trackingId");
            query.setParameter("trackingId", id);
            result = query.list();
            tx.commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }
        return (result != null && !result.isEmpty()) ? result.get(0) : null;
    }

    public List<Tracking> getTrackingByPackageId(int packageId){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<Tracking> result = new ArrayList<Tracking>();
        try{
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Tracking WHERE package_id=:trackingId");
            query.setParameter("trackingId", packageId);
            result = query.list();
            tx.commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }
        return result;
    }

    public Tracking addTracking(Tracking trk){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        int trackingId = -1;
        try{
            tx = session.beginTransaction();
            trackingId = (Integer)session.save(trk);
            trk.setCityId(trackingId);
            tx.commit();
        }catch(HibernateException e){
            e.printStackTrace();
        }

        return (trackingId != -1) ? trk : null;
    }

    public Tracking updateTracking(Tracking trk){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        Tracking cTrk = null;
        try{
            tx = session.beginTransaction();
            session.update(trk);
            tx.commit();
            cTrk = getTrackingById(trk.getId());
        }catch(HibernateException e){
            e.printStackTrace();
        }
        return cTrk;
    }

    public boolean deleteTracking(int id){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        Tracking trk = getTrackingById(id);
        try{
            tx = session.beginTransaction();
            session.delete(trk);
            tx.commit();
            return true;
        }catch(HibernateException e) {
            e.printStackTrace();
        }
        return false;
    }

}
