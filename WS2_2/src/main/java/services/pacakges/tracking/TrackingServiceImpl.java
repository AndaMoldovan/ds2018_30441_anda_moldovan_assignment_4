package services.pacakges.tracking;

import dao.TrackingDao;
import entities.Tracking;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.jws.WebService;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

@WebService(endpointInterface = "services.pacakges.tracking.TrackingService")
public class TrackingServiceImpl implements TrackingService{

    private static TrackingDao trackingDao;
    private static SessionFactory sessionFactory;

    static{
        sessionFactory = new Configuration().configure().buildSessionFactory();
        trackingDao = new TrackingDao(sessionFactory);
    }

    @Override
    public ArrayList<Tracking> getAllTrackings() {
        return (ArrayList<Tracking>) trackingDao.getAllTrackings();
    }

    @Override
    public Tracking getTrackingById(int id) {
        return trackingDao.getTrackingById(id);
    }

    @Override
    public ArrayList<Tracking> getTrackingsByPackageId(int packageId) {
        return (ArrayList<Tracking>) trackingDao.getTrackingByPackageId(packageId);
    }

    @Override
    public Tracking addTracking(int packageId, int cityId, Date time) {
        Tracking tracking = new Tracking();
        tracking.setPackageId(packageId);
        tracking.setCityId(cityId);
        tracking.setTime(time);
        return trackingDao.addTracking(tracking);
    }

    @Override
    public Tracking updateTracking(int id, int packageId, int cityId, Date time) {
        Tracking tracking = new Tracking();
        tracking.setId(id);
        tracking.setPackageId(packageId);
        tracking.setCityId(cityId);
        tracking.setTime(time);
        return trackingDao.addTracking(tracking);
    }

    @Override
    public boolean deleteTracking(int id) {
        return trackingDao.deleteTracking(id);
    }
}
