package services.pacakges.tracking;

import entities.Tracking;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface TrackingService {

    @WebMethod ArrayList<Tracking> getAllTrackings();

    @WebMethod Tracking getTrackingById(int id);

    @WebMethod ArrayList<Tracking> getTrackingsByPackageId(int packageId);

    @WebMethod Tracking addTracking(int packageId, int cityId, Date time);

    @WebMethod Tracking updateTracking(int id, int packageId, int cityId, Date time);

    @WebMethod boolean deleteTracking(int id);
}
