package services.pacakges.user;

import dao.UserDao;
import entities.User;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.jws.WebService;

@WebService(endpointInterface = "services.pacakges.user.UserService")
public class UserServiceImpl implements UserService {

    private static UserDao userDao;
    private static SessionFactory sessionFactory;

    static{
        sessionFactory = new Configuration().configure().buildSessionFactory();
        userDao = new UserDao(sessionFactory);
    }


    @Override
    public User getUserById(int id) {
        return userDao.getUserById(id);
    }

    @Override
    public User login(String username, String password) {
        return userDao.getUserByUsernameAndPassword(username, password);
    }

    @Override
    public User addUser(String username, String password) {
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        return userDao.addUser(user);
    }
}
