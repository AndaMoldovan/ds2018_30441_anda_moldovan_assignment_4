package services.pacakges.user;


import entities.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface UserService {

    @WebMethod User getUserById(int id);

    @WebMethod User login(String username, String password);

    @WebMethod User addUser(String username, String password);

}
