package services.pacakges.cities;

import dao.CityDao;
import entities.City;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService(endpointInterface = "services.pacakges.cities.CityService")
public class CityServiceImpl implements CityService{

    private static CityDao cityDao;
    private static SessionFactory sessionFactory;

    static{
        sessionFactory = new Configuration().configure().buildSessionFactory();
        cityDao = new CityDao(sessionFactory);
    }

    @Override
    public ArrayList<City> getAllCities() {
        return (ArrayList<City>) cityDao.getAllCities();
    }

    @Override
    public City getCityById(int id) {
        return cityDao.getCityById(id);
    }

    @Override
    public City addCity(String name, String countryCode) {
        City city = new City();
        city.setName(name);
        city.setCountryCode(countryCode);
        return cityDao.addCity(city);
    }

    @Override
    public City updateCity(int id, String name, String countryCode) {
        City city = new City();
        city.setId(id);
        city.setName(name);
        city.setCountryCode(countryCode);
        return cityDao.addCity(city);
    }

    @Override
    public boolean deleteCity(int id) {
        return cityDao.deleteCity(id);
    }
}
