package services.pacakges.cities;

import entities.City;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;
import java.util.List;

@WebService
@SOAPBinding(style= SOAPBinding.Style.RPC)
public interface CityService {

    @WebMethod
    ArrayList<City> getAllCities();

    @WebMethod City getCityById(int id);

    @WebMethod City addCity(String name, String countryCode);

    @WebMethod City updateCity(int id, String name, String countryCode);

    @WebMethod boolean deleteCity(int id);
}
