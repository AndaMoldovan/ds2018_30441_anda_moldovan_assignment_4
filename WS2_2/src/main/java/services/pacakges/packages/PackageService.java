package services.pacakges.packages;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;
import java.util.List;

import entities.Package;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface PackageService {

    @WebMethod
    ArrayList<Package> getAllPackaes();

    @WebMethod Package getPackageById(int id);

    @WebMethod ArrayList<Package> getPackagesBySender(int senderId);

    @WebMethod ArrayList<Package> getPackagesByReceiver(int receiverId);

    @WebMethod Package addPackage(int senderId, int receiverId, String name, String destination, int senderCityId, int destinationCityId, int tracking);

    @WebMethod Package updatePackage(int id, int senderId, int receiverId, String name, String destination, int senderCityId, int destinationCityId, int tracking);

    @WebMethod boolean deletePackage(int id);
}
