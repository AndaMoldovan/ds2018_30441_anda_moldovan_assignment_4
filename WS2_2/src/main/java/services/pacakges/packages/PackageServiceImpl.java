package services.pacakges.packages;

import dao.PackageDao;
import entities.Package;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.jws.WebService;

import java.util.ArrayList;

@WebService(endpointInterface = "services.pacakges.packages.PackageService")
public class PackageServiceImpl implements PackageService{

    private static PackageDao packageDao;
    private static SessionFactory sessionFactory;

    static{
        sessionFactory = new Configuration().configure().buildSessionFactory();
        packageDao = new PackageDao(sessionFactory);
    }

    @Override
    public ArrayList<Package> getAllPackaes() {
        return (ArrayList<Package>) packageDao.getAllPackages();
    }

    @Override
    public Package getPackageById(int id) {
        return packageDao.getPackageById(id);
    }

    @Override
    public ArrayList<Package> getPackagesBySender(int senderId) {
        return (ArrayList<Package>) packageDao.getPackagesBySenderId(senderId);
    }

    @Override
    public ArrayList<Package> getPackagesByReceiver(int receiverId) {
        return (ArrayList<Package>) packageDao.getPackagesByReceiverId(receiverId);
    }

    @Override
    public Package addPackage(int senderId, int receiverId, String name, String destination, int senderCityId, int destinationCityId, int tracking) {
        Package pkg = new Package();
        pkg.setSenderId(senderId);
        pkg.setReceiverId(receiverId);
        pkg.setName(name);
        pkg.setDestination(destination);
        pkg.setSenderCityId(senderCityId);
        pkg.setDestinationCityId(destinationCityId);
        pkg.setTracking(tracking);
        return packageDao.addPackage(pkg);
    }

    @Override
    public Package updatePackage(int id, int senderId, int receiverId, String name, String destination, int senderCityId, int destinationCityId, int tracking) {
        Package pkg = new Package();
        pkg.setId(id);
        pkg.setSenderId(senderId);
        pkg.setReceiverId(receiverId);
        pkg.setName(name);
        pkg.setDestination(destination);
        pkg.setSenderCityId(senderCityId);
        pkg.setDestinationCityId(destinationCityId);
        pkg.setTracking(tracking);
        return packageDao.addPackage(pkg);
    }

    @Override
    public boolean deletePackage(int id) {
        return packageDao.deletePackage(id);
    }
}
